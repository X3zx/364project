peano(z). %% zero

peano(Z) :- %% every number after zero is a successor
  successor(Y, X), peano(Y).
  
successor(s(A), A).
%% A + 0 = A
add(A, z, A).
%% s(C) = A + S(B) if C = A + B
add(A, s(B), s(C)) :-
  add(A, B, C).
%% 3 + 2 = 5
add(s(s(s(z))), s(s(z)), S).
%% P + 2 = S
add(s(s(z)), P, S).
%% P = z,
%% S = s(s(z));
%% P = s(z),
%% S = s(s(s(z)));
%% P = s(s(z)),
%% S = s(s(s(s(z))));
%% IT SOLVES UNTIL YOU TELL IT TO STOP. !!!!!

%% A + B = 4
add(A, B, s(s(s(s(z))))).
%% A = s(s(s(s(z)))),
%% B = z;
%% A = s(s(s(z))),
%% B = s(z);
%% A = B, B = s(s(z));
%% A = s(z),
%% B = s(s(s(z)));
%% A = z,
%% B = s(s(s(s(z))));
%% This is fucking amazing

product(z, B, z).
product(s(z), B, B).
product(s(A), B, Product) :-
  add(B, SubProduct, Product),
  product(A, B, SubProduct).
%% This also just happens to be the implementation of division,
%% depending on the arguments being passed into it




sub(X,Y,Z) :- add(Y,Z,X).
lessthan(X,Y) :- add(X,s(_),Y).













